package com.zuitt.batch212;

import java.util.Scanner;

public class VoterVerifier {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Voter age verifier");
        System.out.println("Input your age : ");
        int age = appScanner.nextInt();

        if (age >= 18) {
            System.out.println("You are qualified to vote! Vote wisely! ");
        } else {
            System.out.println("Sorry. Your age is below 18. You are not qualified to vote.");
        }

    }

}
