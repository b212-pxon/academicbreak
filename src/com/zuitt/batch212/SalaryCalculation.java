package com.zuitt.batch212;

import java.util.Scanner;

public class SalaryCalculation {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Salary Calculator");
        System.out.println("Input first name");
        String firstName = appScanner.nextLine().trim();
        System.out.println("Input last name");
        String lastName = appScanner.nextLine().trim();
        System.out.println("How many days did you work?");
        int days = appScanner.nextInt();
        System.out.println("What is your daily rate");
        double rate = appScanner.nextDouble();
        String fullName = firstName + " " + lastName;
        Double salary = days * rate;

        System.out.println("Hi " + fullName + ".");
        System.out.println("Your computed salary is " + salary);

    }

}
