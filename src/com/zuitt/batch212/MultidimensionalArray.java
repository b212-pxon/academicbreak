package com.zuitt.batch212;

import java.util.Arrays;

public class MultidimensionalArray {

    public static void main(String[] args) {

        System.out.println("Favorite Singers/Band: ");
        String[][] artist = { {"Aimer"}, {"Yorushika"}, {"Urbandub"} };
        System.out.println(Arrays.deepToString(artist));
        for (int i = 0; i < artist.length; i++) {
            for (int j = 0; j < artist[i].length; j++) {
                System.out.println(artist[i][j]);
            }
        }

    }

}
