package com.zuitt.batch212;

import java.util.Scanner;

public class SecretWords {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Secret Words");
        System.out.println("Input a number: ");
        int inputNumber = appScanner.nextInt();

        if ( inputNumber == 143) {
            System.out.println("I love you!");
        } else {
            System.out.println("Wrong Guess!");
        }

    }

}
