package com.zuitt.batch212;

import java.util.Scanner;

public class DaySelector {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Day of the week selector");
        System.out.println("Please input a number from 1-7");
        int day = appScanner.nextInt();

        switch (day) {

            case 1:
                System.out.println("Monday.");
                break;

            case 2:
                System.out.println("Tuesday.");
                break;

            case 3:
                System.out.println("Wednesday.");
                break;

            case 4:
                System.out.println("Thursday.");
                break;

            case 5:
                System.out.println("Friday.");
                break;

            case 6:
                System.out.println("Saturday.");
                break;

            case 7:
                System.out.println("Sunday.");
                break;

            default:
                System.out.println("Invalid day. Please try again.");
        }

    }

}
