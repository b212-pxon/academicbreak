package com.zuitt.batch212;

import java.util.Scanner;

public class TriangleArea {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Computing the area of the triangle");
        System.out.println("Input the base");
        double base = appScanner.nextDouble();
        System.out.println("Input the height");
        double height = appScanner.nextDouble();
        double area = 0.5 * ( base * height );

        System.out.println("The area of the triangle is " + area);

    }

}
