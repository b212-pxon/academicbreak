package com.zuitt.batch212;

import java.util.Scanner;

public class YearLevelChecker {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Year level checker");
        System.out.println("Enter your year level: ");
        int yr = appScanner.nextInt();

        switch (yr) {

            case 1 :
                System.out.println("You are a Freshman.");
                break;

            case 2 :
                System.out.println("You are a Sophomore.");
                break;

            case 3 :
                System.out.println("You are a Junior.");
                break;

            case 4 :
                System.out.println("You are a Senior.");
                break;

            default:
                System.out.println("Please input a valid year level.");
        }


    }

}
