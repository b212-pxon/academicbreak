package com.zuitt.batch212;

import java.util.Scanner;

public class CircleArea {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);
        double pi = 3.1416;

        System.out.println("Computing the Area of the circle");
        System.out.println("Input the radius: ");
        double radius = appScanner.nextDouble();
        double area = pi * ( radius * radius );

        System.out.println("The area of the circle is " + area);

    }

}
