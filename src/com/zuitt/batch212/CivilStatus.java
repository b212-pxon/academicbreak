package com.zuitt.batch212;

import java.util.Scanner;

public class CivilStatus {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Civil Status Checker");
        System.out.println("Are you married? (true or false)");
        boolean isMarried = appScanner.nextBoolean();
        System.out.println("Are you separated? (true or false)");
        boolean isSeparated = appScanner.nextBoolean();
        System.out.println("If Yes, Are you legally separated? (true or false)");
        boolean isAnnulled = appScanner.nextBoolean();
        System.out.println("Is your partner still alive? (true or false)");
        boolean isPartnerAlive = appScanner.nextBoolean();

        if (!isMarried) {
            System.out.println("You are single.");
        } else if (isSeparated && isAnnulled) {
            System.out.println("You are annulled.");
        } else if (isSeparated) {
            System.out.println("You are separated.");
        } else if (!isPartnerAlive) {
            System.out.println("You are a widow.");
        } else {
            System.out.println("You are Married.");
        }
    }

}
