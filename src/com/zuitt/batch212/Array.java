package com.zuitt.batch212;

import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

        System.out.println("Names: ");
        String[] names = { "Shinei", "Vladilena", "Anju", "Theo", "Raiden", "Kurena" };
        System.out.println(Arrays.toString(names));
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

    }

}
