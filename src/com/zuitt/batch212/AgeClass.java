package com.zuitt.batch212;

import java.util.Scanner;

public class AgeClass {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Age Class");
        System.out.println("Input age: ");
        int age = appScanner.nextInt();

        if (age > 18 && age < 40) {
            System.out.println("Your age is between 19 to 39 yrs old.");
        } else if (age < 50 && age >= 40 ) {
            System.out.println("Your age is between 40 to 49 yrs old.");
        } else if ( age >= 50 ) {
            System.out.println("Your age is 50 or above 50 yrs old.");
        } else {
            System.out.println("Your age is 18 or below 18 yrs old.");
        }

    }

}
