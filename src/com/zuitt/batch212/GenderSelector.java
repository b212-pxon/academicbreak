package com.zuitt.batch212;

import java.util.Scanner;

public class GenderSelector {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Gender Selector");
        System.out.println("Please input \"m\" or \"f\"");
        String gender = appScanner.nextLine().toLowerCase();

        switch (gender) {

            case "m":
                System.out.println("Hello Sir!");
                break;

            case "f":
                System.out.println("Hello Madam!");
                break;

            default:
                System.out.println("Please input \"m\" or \"f\" only.");
        }
    }

}
