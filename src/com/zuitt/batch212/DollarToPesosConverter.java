package com.zuitt.batch212;

import java.util.Scanner;

public class DollarToPesosConverter {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("Dollar to PHP Converter");
        System.out.println("Input how many dollars: ");
        double dollars = appScanner.nextDouble();
        double php = dollars * 56.16;

        System.out.println("The converted value is ₱ " + php);

    }

}
